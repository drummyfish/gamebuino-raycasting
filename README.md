## Gamebuino META raycastlib demos

![](https://gitlab.com/drummyfish/raycastlib/raw/master/media/gamebuino.gif)

This is a port of raycasting demos, which I originally made for Pokitto, to Gamebuino META. The original demos can be found [here](https://gitlab.com/drummyfish/Pokitto-Raycasting).

The demos are using my own library [raycastlib](https://gitlab.com/drummyfish/raycastlib).

# Quick how-to

**Don't forget to compile with -O3 compiler optimization flag!** (otherwise it's going to be slow). Use a search engine to find out how to turn these on (see e.g. [here](https://forum.arduino.cc/index.php?topic=343730.0)) and don't forget to check the compiler is actually using them in the compiler console output!

You can choose which demo to compile in the main *.ino* file with the include.

Start with *helloRay* hello world program to see the basics.

Most things are documented in the code comments. You can find even more documentation in the [repo with the original demos](https://gitlab.com/drummyfish/Pokitto-Raycasting) and [raycastlib repo](https://gitlab.com/drummyfish/raycastlib).
More info related to the GameBuino port can be found in the release post [here](https://gamebuino.com/creations/advanced-raycasting).

Don't be afraid to ask away.

# License

Everything is CC0 1.0. **Please share your own software as free and open-source.** Credit is also appreciated, but not required.



